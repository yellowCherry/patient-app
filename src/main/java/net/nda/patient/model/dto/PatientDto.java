package net.nda.patient.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.nda.patient.model.entity.Patient;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class PatientDto {
    private Long id;
    private String name;
    private String gender;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    public static PatientDto of(Patient patient) {
        return new PatientDto(
                patient.getId(),
                patient.getName(),
                patient.getGender(),
                patient.getBirthDate()
        );
    }
}

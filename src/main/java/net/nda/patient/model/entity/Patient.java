package net.nda.patient.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Patient {

    @Id
    @SequenceGenerator(name = "patient_id_seq",
            sequenceName = "patient_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "patient_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;
    private String name;
    private String gender;
    private LocalDate birthDate;

    public Patient(String name, String gender, LocalDate birthDate) {
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
    }
}

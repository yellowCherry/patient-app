package net.nda.patient.service;

import net.nda.patient.model.entity.Patient;

public interface PatientService {
    Long save(Patient patient);

    Patient update(Long id, Patient patient);

    Patient getPatientById(Long id);

    void deletePatient(Long id);

}

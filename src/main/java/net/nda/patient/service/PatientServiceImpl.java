package net.nda.patient.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import net.nda.patient.model.entity.Patient;
import net.nda.patient.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    public Long save(Patient patient) {
        return patientRepository.save(patient).getId();
    }

    public Patient update(Long id, Patient updatedPatient) {
        Patient currentPatient = patientRepository.getReferenceById(id);
        currentPatient.setName(updatedPatient.getName());
        currentPatient.setGender(updatedPatient.getGender());
        currentPatient.setBirthDate(updatedPatient.getBirthDate());
        return currentPatient;
    }

    @Override
    public Patient getPatientById(Long id) {
        return patientRepository.getReferenceById(id);
    }

    @Override
    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }
}

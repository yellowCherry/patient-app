package net.nda.patient.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.nda.patient.model.dto.PatientDto;
import net.nda.patient.model.entity.Patient;
import net.nda.patient.service.PatientService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/patient")
@RequiredArgsConstructor
public class PatientController {

    private final PatientService patientService;

    @GetMapping("/{id}")
    @Operation(summary = "Get patient by ID")
    @PreAuthorize("hasAnyAuthority('Practitioner', 'Patient')")
    public ResponseEntity<PatientDto> getPatient(@PathVariable Long id, Principal principal) {
        log.info("Principal name: " + principal.getName());
        Patient patient = patientService.getPatientById(id);
        PatientDto patientDto = PatientDto.of(patient);
        return ResponseEntity.ok(patientDto);
    }

    @PostMapping
    @Operation(summary = "Create patients")
    @PreAuthorize("hasAuthority('Practitioner')")
    public ResponseEntity<Long> savePatient(@RequestBody PatientDto patientDto) {
        Patient patient = new Patient(patientDto.getName(), patientDto.getGender(), patientDto.getBirthDate());
        Long id = patientService.save(patient);
        return ResponseEntity.ok(id);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update patient")
    @PreAuthorize("hasAuthority('Practitioner')")
    public ResponseEntity<PatientDto> updatePatient(@PathVariable Long id, @RequestBody PatientDto patientDto) {
        Patient patient = new Patient(patientDto.getName(), patientDto.getGender(), patientDto.getBirthDate());
        Patient updatedPatient = patientService.update(id, patient);
        PatientDto updatedPatientDto = PatientDto.of(updatedPatient);
        return ResponseEntity.ok(updatedPatientDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete patient")
    @PreAuthorize("hasAuthority('Practitioner')")
    public ResponseEntity<Void> deletePatient(@PathVariable Long id) {
        patientService.deletePatient(id);
        return ResponseEntity.noContent().build();
    }
}
